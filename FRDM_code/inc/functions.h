// This file is a handler for main function from the calculator

#include "frdm_bsp.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include <cstring>


void delay_ms(int delay);	// Setting up very simple delay function 

void row_init(int ROW);	// Function initializing row of the keyboard

int calculate(char input_string[]); // Declaration of the calculating function

void LCD_startup(); // Initialization and 'title screen'
