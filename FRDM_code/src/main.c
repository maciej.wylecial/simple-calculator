#include "MKL05Z4.h" /*Device header*/
#include "lcd1602.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "functions.h"

// Setting up pinout for 4x4 keyboard just like in Arduino
#define C1 8
#define C2 7
#define C3 6
#define C4 5

#define R1 12
#define R2 11
#define R3 10
#define R4 9

// Defining keyboard Layout, thanks for using it we can set functions up using symbols instead of pins
char BUTTONS[] = {'7', '8', '9', '/', 
									'4', '5', '6', '*', 
									'1', '2', '3', '-', 
									'C', '0', '=', '+'};
	int COLUMNS[] =  { C4,  C3,  C2,  C1 };
	int ROWS[] =	 { R4,  R3,  R2,  R1 };


void pins_setup() {
	// Enabling clock usage for both ports A and B
	SIM->SCGC5 |= SIM_SCGC5_PORTA_MASK;
	SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK; 
	
	// Setting up MUX pins of the keyboard as a GPIO
	for(int i = 0; i < 4; i++)
		PORTA->PCR[COLUMNS[i]] |= PORT_PCR_MUX(1); 
	
	for(int i = 0; i < 4; i++)
		PORTA->PCR[ROWS[i]] |= PORT_PCR_MUX(1);
	
	// Setting output on Row pins
	for(int i = 0; i < 4; i++)
		PTA->PDDR |= (1<<ROWS[i]);
	
	// Setting up high on Row pins
	for(int i = 0; i < 4; i++)
		PTA->PSOR |= (1<<ROWS[i]);
	
	// Enabling pull-up resistors to have accurate readings of the keyboard
	for(int i = 0; i < 4; i++) {
		PORTA->PCR[COLUMNS[i]] |= PORT_PCR_PE_MASK | PORT_PCR_PS_MASK;
	}
}

int main (void) {
	
	pins_setup();
	
	// Initializing of the LCD panel
	LCD_startup();
	
	char button = '0'; // Handler for button reading
	char output_string[16] = "";	// Output handler  (it's a char as c doesn't support strings)
	int restart = 0;
	char operator = '0';
	// Main working loop 
	while(1){
			
			int pressed_debug = 0;
			// Algorithm for checking actual state of the whole keyboard
			// One loop is for columns and the second one checks rows
			for(int c = 0; c < 4; c++){
					
				row_init(ROWS[c]);
				
				for(int r = 0; r < 4; r++){
					
					// Disable possibility of operator as a first input to avoid errors
					if( (strlen(output_string) == 0 ) && r == 3 )
						continue;
					
					// Blocking multiple operators
					if( operator != '0' && r == 3)
						continue;
					
					if ( restart == 1 ){
						if ( !(r == 0 && c == 3) )
							continue;
					}
					
					// Button is pressed
					if( ( PTA->PDIR & (1<<COLUMNS[r]) ) == 0){						
						
						if(pressed_debug){

							break;
						}
						else{
							button = BUTTONS[4 * c + r];
							pressed_debug = 1;	

							if ( r == 3 )
								operator = button;		

							/* '=' button behaviour */
							if( button == '=' ){
								
								/* Do nothing when output_string empty */
								if( strlen(output_string) == 0  || operator == '0' )
									continue;
								
								strncat(output_string, &button, 1);
								
								char output_number[16];
								int temp = calculate(output_string);
								sprintf(output_number, "%d", temp);

								LCD1602_SetCursor(0,1);
								LCD1602_Print(output_number);
								
								restart = 1;
							}							
							
							// Setting up clear button
							if( button == 'C' ){
								
								// If there is no inpute just do nothing
								if( strlen(output_string) == 0 ) {
									continue; 
								}
								// If there is some input AC - clear all
								char temp = output_string[strlen(output_string) - 1];
							  memset(output_string, 0, sizeof(output_string));
								operator = '0';
								
								/* If last character was '=' operator - clear end_of_duty flag*/
								if( temp == '=' ) {
									restart = 0;
								}
							
								LCD1602_ClearAll();
							}
							else if (restart == 0) /* Concatenate the character corresponding to pressed button to output_string */
								strncat(output_string, &button, 1);
					
						}
					}
				}
			}
			row_init(0);
			LCD1602_SetCursor(0,0);
			LCD1602_Print(output_string);
		}
}

