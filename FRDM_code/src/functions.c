#include "functions.h"
#include "lcd1602.h"

#define C1 8
#define C2 7
#define C3 6
#define C4 5

#define R1 12
#define R2 11
#define R3 10
#define R4 9


void delay_ms(int delay){
	
	int i;
	int j;
	
	for( i = 0 ; i < delay; i++) {
		for(j = 0; j < 6000; j++) {}	// This value gives us delay close to 1ms
	}
}

void row_init(int ROW){
	
	PTA->PSOR |= (1<<R4); 
	PTA->PSOR |= (1<<R3);
	PTA->PSOR |= (1<<R2);
	PTA->PSOR |= (1<<R1); 
	
	if(ROW == R4 || ROW == R3 || ROW == R2 || ROW == R1)
		PTA->PCOR |= (1<<ROW); 		
	delay_ms(30); //Lower values can print multiplied values intead of single
}

void LCD_startup() {
	LCD1602_Init(); 
	LCD1602_Backlight(TRUE);
	LCD1602_SetCursor(0,0);
	LCD1602_Print("Initialization");
	delay_ms(1000);
	LCD1602_ClearAll();
	LCD1602_SetCursor(0,0);
	LCD1602_Print("Calculator");
	delay_ms(1000);
	LCD1602_ClearAll();
}


// This function contains calculating logic
int calculate(char input[]) {

	int out = 0; // handler for the output of the executed operation
	char first[16] = "";
	char second[16] = "";
	int operator_index = 0;
	int execute = 0;
	char operator = '0';

	for ( unsigned int i = 0; i < strlen(input) ; i++) {

    if ( input[i] == '=' ) { // checking if the operation is ready to be exexuted
        execute = i;
        break;
    }

		if ( input[i] == '+' || input[i] == '-' || input[i] == '*' || input[i] == '/' ){ // Checking input from keyboard to set mathematical operator
			operator_index = i;
			operator = input[i];
		}
	} 
	
	for(int i = 0; i < operator_index; i++ ) {
     strncat(&first[i], &input[i], 1); // This function is used to merge two chains of chars
	}
	for(int i = 0; i < execute - operator_index; i++ ) {
     strncat(&second[i], &input[i + operator_index + 1], 1);
	}
	
	int first_number_value = atoi(first);
    int second_number_value = atoi(second);
	// This is the whole logic behind calculating
	if (operator == '+') 
		out = first_number_value + second_number_value;
	else if (operator == '-')
		out = first_number_value - second_number_value;
	else if (operator == '*')
		out = first_number_value * second_number_value;
	else if (operator == '/')
		out = first_number_value / second_number_value;

	return out; // returning executed operation
}
